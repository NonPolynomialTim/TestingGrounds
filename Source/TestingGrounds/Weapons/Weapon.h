// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Weapon.generated.h"

class ABallProjectile;
class UAnimInstance;
class UAnimMontage;

UCLASS()
class TESTINGGROUNDS_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeapon();

	/** Fires a projectile. */
	UFUNCTION(BlueprintCallable)
	void Fire();

	/** Gun mesh: 1st person view (seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* FP_Gun;

	/** Gun mesh: VR view (attached to the VR controller directly, no arm, just the actual gun) */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USkeletalMeshComponent* VR_Gun;

	/** Projectile class to spawn */
	UPROPERTY(EditDefaultsOnly, Category = Projectile)
	TSubclassOf<class ABallProjectile> ProjectileClass;

	/** Location on gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* FP_MuzzleLocation;

	/** Location on VR gun mesh where projectiles should spawn. */
	UPROPERTY(VisibleDefaultsOnly, Category = Mesh)
	class USceneComponent* VR_MuzzleLocation;

	/** Sound to play each time we fire */
	UPROPERTY(EditDefaultsOnly, Category = Gameplay)
	class USoundBase* FireSound;

	UPROPERTY(EditDefaultsOnly, Category = "Animations")
	UAnimMontage* FPFireRifleAnimMontage;

	UPROPERTY(EditDefaultsOnly, Category = "Animations")
	UAnimMontage* TPFireRifleAnimMontage;

	UAnimInstance* FPAnimInstance;

	UAnimInstance* TPAnimInstance;

	APawn* Owner;
protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
};
