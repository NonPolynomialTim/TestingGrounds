// Fill out your copyright notice in the Description page of Project Settings.

#include "InfiniteTerrainGameMode.h"
#include "UI/TestingGroundsHUD.h"
#include "Character/Player/PlayerCharacter.h"
#include "UObject/ConstructorHelpers.h"
#include "AI/Navigation/NavMeshBoundsVolume.h"
#include "Kismet/GameplayStatics.h"
#include "Environment/ActorPool.h"

AInfiniteTerrainGameMode::AInfiniteTerrainGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/Dynamic/Character/Behavior/BP_Mannequin"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = ATestingGroundsHUD::StaticClass();

	NavMeshPool = CreateDefaultSubobject<UActorPool>(FName("NavMeshPool"));
}

void AInfiniteTerrainGameMode::AddToScore(int ScoreToAdd)
{
	Score += ScoreToAdd;
}

void AInfiniteTerrainGameMode::PopulateNavMeshPool()
{
	TArray<AActor*> NavMeshes;

	UGameplayStatics::GetAllActorsOfClass(GetWorld(), ANavMeshBoundsVolume::StaticClass(), NavMeshes);

	for (int32 i = 0; i < NavMeshes.Num(); i++)
	{
		NavMeshPool->CheckIn(NavMeshes[i]);
	}
}