// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "Mannequin.generated.h"

class UInputComponent;
class UCameraComponent;
class AWeapon;

UCLASS()
class TESTINGGROUNDS_API AMannequin : public ACharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	AMannequin();

	void UnPossessed() override;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION(BlueprintCallable, Category = "Firing")
	void PullTrigger();

	UFUNCTION(BlueprintCallable, Category = "Utilities")
	void ReparentWeapon(USkeletalMeshComponent* NewParentMesh, FName SocketName);

	// Default Weapon Class
	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	TSubclassOf<AWeapon> DefaultWeaponClass;

	//Camera
	UPROPERTY(BlueprintReadWrite, Category = "Components")
	UCameraComponent* Camera;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	USkeletalMeshComponent* FPSkeletalMesh;

	UPROPERTY(VisibleAnywhere, Category = "Components")
	AWeapon* Weapon;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) override;

	
	
};
