// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "GenericTeamAgentInterface.h"
#include "TestingGroundsPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API ATestingGroundsPlayerController : public APlayerController, public IGenericTeamAgentInterface
{
	GENERATED_BODY()

	ATestingGroundsPlayerController();

public:
	virtual void SetPawn(APawn* InPawn) override;

private:
	UFUNCTION()
	void KillPlayer();
};
