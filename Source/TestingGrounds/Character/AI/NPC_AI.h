// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "AIController.h"
#include "NPC_AI.generated.h"

/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API ANPC_AI : public AAIController
{
	GENERATED_BODY()

	ANPC_AI();

	void SetPawn(APawn* InPawn) override;

private:
	UPROPERTY(EditDefaultsOnly)
	UAIPerceptionComponent* AIPerceptionComponent;

	FGenericTeamId TeamID;

	UFUNCTION()
	void KillPawn();
};
