// Fill out your copyright notice in the Description page of Project Settings.

#include "FocusOnPoint.h"
#include "AIController.h"
#include "BehaviorTree/BlackboardComponent.h"

EBTNodeResult::Type UFocusOnPoint::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	UBlackboardComponent* BlackboardComponent = OwnerComp.GetBlackboardComponent();
	BlackboardComponent->SetValueAsObject(KFocusPoint.SelectedKeyName, GetWorld()->GetFirstPlayerController()->GetPawn());
	OwnerComp.GetAIOwner()->SetFocus(Cast<AActor>(BlackboardComponent->GetValueAsObject(KFocusPoint.SelectedKeyName)));

	return EBTNodeResult::Succeeded;
}
