// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTTaskNode.h"
#include "FocusOnPoint.generated.h"

/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API UFocusOnPoint : public UBTTaskNode
{
	GENERATED_BODY()
	


	EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;

private:
	UPROPERTY(EditAnywhere, Category = "Blackboard")
	FBlackboardKeySelector KFocusPoint;
};
