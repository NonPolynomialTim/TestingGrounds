// Fill out your copyright notice in the Description page of Project Settings.

#include "PatrollingGuard.h"
#include "Weapons/Weapon.h"
#include "Engine/World.h"


// Sets default values
APatrollingGuard::APatrollingGuard()
{
	SetGenericTeamId(1);
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	CurrentHealth = MaxHealth;
}

// Called when the game starts or when spawned
void APatrollingGuard::BeginPlay()
{
	Super::BeginPlay();

	USkeletalMeshComponent* CharacterMesh = FindComponentByClass<USkeletalMeshComponent>();

	if (!CharacterMesh)
	{
		UE_LOG(LogTemp, Error, TEXT("A PatrollingGuard is missing a SkeletalMesh Component!"));
		return;
	}

	if (!DefaultWeaponClass)
	{
		UE_LOG(LogTemp, Error, TEXT("A PatrollingGuard is missing a Default Weapon Class!"));
		return;
	}

	GuardWeapon = GetWorld()->SpawnActor<AWeapon>(
		DefaultWeaponClass,
		FVector(0),
		FRotator(0));

	GuardWeapon->AttachToComponent(CharacterMesh, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, true), FName("GripPoint"));
	
}

// Called every frame
void APatrollingGuard::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

// Called to bind functionality to input
void APatrollingGuard::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void APatrollingGuard::FireWeapon()
{
	if (!GuardWeapon)
	{
		UE_LOG(LogTemp, Error, TEXT("A PlayerCharacter is missing a weapon!"));
		return;
	}

	GuardWeapon->Fire();
}

float APatrollingGuard::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, AController* EventInstigator, AActor* DamageCauser)
{
	Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);

	UE_LOG(LogTemp, Warning, TEXT("%s taking damage!"), *(GetName()));

	float PreviousHealth = CurrentHealth;
	CurrentHealth -= Damage;
	if (CurrentHealth <= 0.0f)
	{
		CurrentHealth = 0.0f;
		OnDeath.Broadcast();
		bDead = true;
	}

	return (PreviousHealth - CurrentHealth);
}
