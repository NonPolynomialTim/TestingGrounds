// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "InfiniteTerrainGameMode.generated.h"

class UActorPool;

/**
 * 
 */
UCLASS()
class TESTINGGROUNDS_API AInfiniteTerrainGameMode : public AGameModeBase
{
	GENERATED_BODY()
	
public:
	AInfiniteTerrainGameMode();

	void AddToScore(int ScoreToAdd);

	UFUNCTION(BlueprintCallable, Category = "Utilities")
	int GetScore() const { return Score; }

	UFUNCTION(BlueprintCallable, Category = "Navigation")
	void PopulateNavMeshPool();

	UPROPERTY(BlueprintReadOnly, Category = "Components")
	UActorPool* NavMeshPool;

private:
	int Score = 0;
};
