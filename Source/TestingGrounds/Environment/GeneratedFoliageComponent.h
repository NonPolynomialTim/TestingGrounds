// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/HierarchicalInstancedStaticMeshComponent.h"
#include "GeneratedFoliageComponent.generated.h"

/**
 * 
 */
UCLASS( ClassGroup = (Custom), Meta = (BlueprintSpawnableComponent))
class TESTINGGROUNDS_API UGeneratedFoliageComponent : public UHierarchicalInstancedStaticMeshComponent
{
	GENERATED_BODY()
	
protected:
	void BeginPlay() override;
	
	void GenerateFoliage();

	void SpawnFoliage(FVector Location, FRotator Rotation, float Scale);

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	int32 MinSpawnNumber = 0;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	int32 MaxSpawnNumber = 100;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	bool CanSpawnYawed = true;

	UPROPERTY(EditDefaultsOnly, Category = "Setup")
	bool CanSpawnScaled = true;

	UPROPERTY(EditDefaultsOnly, Meta = (EditCondition = CanSpawnScaled), Category = "Setup")
	float MinScale = 0.75f;

	UPROPERTY(EditDefaultsOnly, Meta = (EditCondition = CanSpawnScaled), Category = "Setup")
	float MaxScale = 1.5f;
};
