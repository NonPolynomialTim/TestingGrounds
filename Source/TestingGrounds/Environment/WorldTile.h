// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "WorldTile.generated.h"

class UActorPool;
class ANavMeshBoundsVolume;

UCLASS()
class TESTINGGROUNDS_API AWorldTile : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWorldTile();

	void EndPlay(const EEndPlayReason::Type EndPlayReason) override;

	UFUNCTION(BlueprintCallable, Category = "Utilities")
	void InitializeNavMeshPool(UActorPool* NavMeshPoolToSet);

protected:
	UFUNCTION(BlueprintCallable, Category = "Level Generation")
	void GenerateEnvironmentActors(TSubclassOf<AActor> ClassToSpawn, int32 MinNumber = 1, int32 MaxNumber = 1, float Radius = 300.0f , float MinScale = 1.0f, float MaxScale = 1.0f, bool CanYaw = true, bool CanPitch = false, bool CanRoll = false);

	UFUNCTION(BlueprintCallable, Category = "Level Generation")
	void GenerateAI(TSubclassOf<APawn> PawnToSpawn, int32 MinNumber = 1, int32 MaxNumber = 1, float Radius = 300.0f);

	UFUNCTION(BlueprintCallable, Category = "Gameplay")
	void ConquerTile();

	void DestroyGeneratedEnvironmentActors();
	
	bool SphereCast(FVector Location, float Radius);

	bool FindFreeLocation(FVector& outFreeLocation, float Radius);

	void SpawnActor(TSubclassOf<AActor> ClassToSpawn, FVector Location, FRotator Rotation, float Scale);

	UPROPERTY(EditAnywhere, Category = "Default")
	bool RequiresNavMesh = true;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Default")
	bool IsStartSquare = false;

	bool IsConquered = false;

private:
	UActorPool* NavMeshPool;

	ANavMeshBoundsVolume* NavMeshBoundsVolume;
};
